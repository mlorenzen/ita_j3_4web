package model.shop.command;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {
	private String commandName = null;
	private String viewName = null;
	private List<String> successor = new ArrayList<String>();

	public Command(String commandName, String viewName, String ... commands) {
		super();
		this.commandName = commandName;
		this.viewName = viewName;
		for (String cmd : commands) {
			this.successor.add(cmd);
		}
	}
	
	public String getName() {
		return this.commandName;
	}

	public String getView() {
		return this.viewName;
	}
	
	public boolean isAllowedSuccessor(String command) {
		return this.successor.contains(command);
	}
	
	public Command executeCommand(CommandContext context) {
		this.execute(context);
		
		if (context.hasForward()) {
			context.resetForward();
			Command cmd = CommandBroker.getInstance().lookupCommand(context.getForwardCommand());
			if (cmd != null) {
				cmd.executeCommand(context);
			}
			return cmd;
		}
		return this;
	}
	
	public abstract void execute(CommandContext context);
}
