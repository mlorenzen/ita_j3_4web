package model.shop;

public enum Ingredients {
	PILZE, THUNFISCH, GORGONZOLA, ZWIEBELN, SALAMI, SCHINKEN, SPINAT, PEPERONI, LACHS;
	
	@Override
	public String toString() {
		return name().substring(0, 1) + name().substring(1).toLowerCase();
	}

}
