<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<title>Michas Pizzaladen - Auswahl</title>
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<script type="application/javascript" src="js/pizza.js"></script>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Stellen Sie Ihre Pizza zusammen</h2>
<form action="shopcontrol" method="get">
  <fieldset>Gr��e der Pizza:
  <select name="pizzaSize" size="1" id="sizeSelect">
  <option value="S" >Klein</option>
  <option selected value="M" >Mittel</option>
  <option value="L" >Gro�</option>
  </select>
  </fieldset>
  <fieldset>
  <label>
  <input type="checkbox" name="zutat" value="PILZE" /> Pilze
  </label>
  <label>
  <input type="checkbox" name="zutat" value="THUNFISCH" /> Thunfisch
  </label>
  <label>
  <input type="checkbox" name="zutat" value="GORGONZOLA" /> Gorgonzola
  </label>
  <label>
  <input type="checkbox" name="zutat" value="ZWIEBELN" /> Zwiebeln
  </label>
  <label>
  <input type="checkbox" name="zutat" value="SALAMI" /> Salami
  </label>
  <label>
  <input type="checkbox" name="zutat" value="SCHINKEN" /> Schinken
  </label>
  <label>
  <input type="checkbox" name="zutat" value="SPINAT" /> Spinat
  </label>
  <label>
  <input type="checkbox" name="zutat" value="PEPERONI" /> Peperoni
  </label>
  <label>
  <input type="checkbox" name="zutat" value="LACHS" /> Lachs
  </label>
  </fieldset>
  <fieldset>Preis der Auswahl: <div id="preisanzeige"></div></fieldset>
  <input class="buttn" type="submit" value="Auswahl in den Warenkorb" />
  <input type="hidden" name="command" value="warenkorb" />
</form>
<hr>
<p>Ihr Benutzername: ${user} </p> 
</div>
</body>
</html>