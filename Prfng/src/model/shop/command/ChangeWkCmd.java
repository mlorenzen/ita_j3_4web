package model.shop.command;

import java.util.ArrayList;

import model.shop.ShopFacade;
import model.shop.Warenkorb;

public class ChangeWkCmd extends Command {
	
	private ShopFacade facade = null;

	public ChangeWkCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
		this.facade = ShopFacade.getInstance();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute(CommandContext context) {
		String orderID = ((ArrayList<String>) context.getAttribute("orderNr")).get(0);
		String changeCmd = ((ArrayList<String>) context.getAttribute("changeCmd")).get(0);
		
		Warenkorb wk = context.getWarenkorb();
		
		switch (changeCmd) {
		case "inc":
			wk.increaseOrder(orderID);
			break;
		case "dec":
			wk.decreaseOrder(orderID);
			break;
		case "del":
			wk.deleteOrder(orderID);
			break;
		}
		
	}

}
