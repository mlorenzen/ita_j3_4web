<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Michas Pizzaladen - Anmeldung</title>
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Bitte melden Sie sich an</h2>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value ="kaufen" >
  <fieldset>
  <label> Benutzername: 
    <input name="username" type="text" size="20">
  </label>
  <br>
  <label> Passwort:
    <input name="passwd" type="password" size="20" >
  </label>
  </fieldset>
  <input type="submit" class="buttn" value="Anmelden" >
</form>
<div class="fehler">${error.message } </div>
<hr>
<p>Noch keine Benutzerdaten?</p>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="registrierung">
  <input type="submit" class="buttn" value="Zur Registrierung">
</form>
</div>
</body>
</html>