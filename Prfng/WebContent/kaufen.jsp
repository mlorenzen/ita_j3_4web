<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<title>Michas Pizzaladen - Kauf abschließen</title>
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Ihre Auswahl</h2>
<table >
<tr>
  <th>Nr.</th>
  <th>Anzahl</th>
  <th>Pizza</th>
  <th>Preis</th>
</tr>
<c:forEach items="${warenkorb.content}" var="b">
  <tr>
    <td>${b.id }</td>
    <td>${b.count } </td>
    <td>${b.pizzaDescription }</td>
    <td>${b.price }</td>
  </tr>
</c:forEach>
</table>
<p>Gesamtpreis: ${warenkorb.price } EUR</p>
<hr>
<div id="offer">special offer goes here</div>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="auswahl">
  <input class="buttn" type="submit" value="Mehr Pizzen auswählen">
</form>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="editWarenkorb">
  <input class="buttn" type="submit" value="Bestellung bearbeiten">
</form>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="danke">
  <input class="buttn" type="submit" value="Jetzt Kaufen">
</form>
<hr>
<p>Ihr Benutzername: ${user} </p> 
</div>
</body>
</html>