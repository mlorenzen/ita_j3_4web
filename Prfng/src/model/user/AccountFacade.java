package model.user;

import java.util.Map;

public class AccountFacade {
	
	private static class Instance {
		private static AccountFacade instance = new AccountFacade();
	}
	
	public static AccountFacade getInstance() {
		return Instance.instance;
	}
	
	private AccountFacade() {
		super();
	}
	private	UserStore store = UserStore.getInstance();
	
	public void saveUser(Map<String, String> newUser) {
		String username = newUser.get("username");
		String firstname = newUser.get("vorname");
		String name = newUser.get("name");
		String address = newUser.get("address");
		int plz = Integer.parseInt(newUser.get("plz"));
		String city = newUser.get("city");
		String telephone = newUser.get("telephone");
		String password = newUser.get("password");
		
		store.addUser(username, firstname, name, address, plz, city, telephone, password);
	}
	
	public boolean validatePassword(String user, String password) {
		return store.testPassword(user, password);
	}
	
	public String getDeliveryAddress(String username) {
		User user = store.getUser(username);
		return (user.getFirstName() + " " + user.getName() + "; " + user.getFullAddress());
	}
	
	public boolean userExists(String username) {
		return (store.getUser(username) == null) ? false : true;
	}
	
}
