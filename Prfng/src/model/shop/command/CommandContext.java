package model.shop.command;

import java.util.HashMap;
import java.util.Set;

import model.shop.Warenkorb;

public class CommandContext {
	private HashMap<String, Object> context = new HashMap<String, Object>();
	private Warenkorb wk;
	private boolean error = false;
	private Exception ex = null;
	private boolean forward = false;
	private String nextCommand = null;
	
	public CommandContext() {
		super();
	}
	
	public void setWarenkorb(Warenkorb wk) {
		this.wk = wk;
	}
	
	public Warenkorb getWarenkorb() {
		return this.wk;
	}
	
	public void setAttribute(String key, Object value) {
		this.context.put(key, value);
	}
	
	public Object getAttribute(String key) {
		return this.context.get(key);
	}
	
	public Set<String> getAttributeNames(){
		return this.context.keySet();
	}
	
	public boolean hasError() {
		return this.error;
	}
	
	public void resetError() {
	   this.error = false;
	}
	
	public void setError(Exception ex) {
	   this.error = true;
	   this.ex = ex;
    }
	   
	public Exception getError() {
	   return this.ex;
	}	
	
	public boolean hasForward() {
		return this.forward;
	}
	
	public void resetForward() {
		this.forward = false;
	}
	
	public void setForwardCommand(String cmd) {
		this.forward = true;
		this.nextCommand = cmd;
	}
	
	public String getForwardCommand() {
		return nextCommand;
	}
}
