window.onload = function() {
	document.getElementById("plzInput").onblur = validatePLZ;
	document.getElementById("telInput").onblur = validateTelephone;
	document.getElementById("pwd1").onblur = validatePw;
	document.getElementById("pwd2").onblur = validatePw;
}

function validateTelephone(){
	var number = document.getElementById("telInput").value;
	//Quelle: http://www.1ngo.de/web/regEx.html
	var telRegEx = /^((\+[0-9]{2,4}([ -][0-9]+?[ -]| ?\([0-9]+?\) ?))|(\(0[0-9 ]+?\) ?)|(0[0-9]+? ?( |-|\/) ?))([0-9]+?[ \/-]?)+?[0-9]$/;
	var errorDiv = document.getElementById("errorContainer");
	
	if (telRegEx.test(number) == false){
		errorDiv.innerHTML = "Format der Telefonnummer falsch, bitte mit Vorwahl eingeben";
	} else {
		errorDiv.innerHTML = ""; 
	}
}

function validatePLZ(){
	var plz = document.getElementById("plzInput").value;
	var telRegEx = /[0-9]{5}/;
	var errorDiv = document.getElementById("errorContainer");
	
	if (telRegEx.test(plz) == false){
		errorDiv.innerHTML = "Format der Postleitzahl falsch";
	} else {
		errorDiv.innerHTML = ""; 
	}
}

function validatePw(){
	var pw1 = document.getElementById("pwd1").value;
	var pw2 = document.getElementById("pwd2").value;
	var errorDiv = document.getElementById("errorContainer");
	
	if (pw1 != pw2){
		errorDiv.innerHTML = "Passw&ouml;rter sind nicht gleich";
	} else {
		errorDiv.innerHTML = ""; 
	}
}