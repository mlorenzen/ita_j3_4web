<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<title>Michas Pizzaladen - Registrierung</title>
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<script type="application/javascript" src="js/registrierung.js"></script>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Als Neukunde registrieren</h2>
<form action="shopcontrol" method="post">
  <input type="hidden" name="command" value="register">
  <fieldset>
    <label>Vorname:
      <input name="vorname" type="text" size="20">
    </label><br>
    <label>Nachname:
      <input name="name" type="text" size="20">
    </label><br>
    <label>Anschrift:
      <input name="address" type="text" size="20">
    </label><br>
    <label>PLZ:
      <input name="plz" type="text" size="5" id="plzInput">
    </label><br>
    <label>Ort:
      <input name="city" type="text" size="20">
    </label><br>
    <label>Telefon:
      <input name="telephone" type="text" size="20" id="telInput">
    </label><br>
    <label>Benutzername:
      <input name="username" type="text" size="20">
    </label><br>
    <label>Passwort:
      <input name="password" type="password" size="20" id="pwd1">
    </label><br>
    <label>Passwort wiederholen:
      <input name="pw2" type="password" size="20" id="pwd2">
    </label>
  </fieldset>
 <input type="submit" class="buttn" id="regbuttn" value="Registrieren"> 
</form>
<form action="shopcontrol" method="get">
<div class="fehler">${error.message } </div>
<div id=errorContainer class="fehler"> </div>
  <input type="hidden" name="command" value="editWarenkorb">
  <input class="buttn" type="submit" value="Zur�ck zum Warenkorb">
</form>
</div>
</body>
</html>