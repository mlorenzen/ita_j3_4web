<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Michas Buchladen - Suchergebnisse</title>
<script type="application/javascript" src="js/suchergebnis.js"></script>
<link rel="stylesheet" type="text/css" href="css/buchladen_ml.css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
<div id="content">
<h1>Michas Buchladen</h1>
<h2>Suchergebnisse</h2>
<p>Es wurden folgende B�cher gefunden: </p> 

<table>
<tr id="tableHeader" ><th>Autor(en)</th><th>Titel</th></tr>
  <c:forEach items="${buecher}" var="b"><td></td><td></td>
  <tr>
    <td>${b.autorenAsString}</td>
    <td><a id='${b.id}' href="detail?buchid=${b.id}"> ${b.titel}</a></td>
  </tr>
  </c:forEach>
</table>
<hr />
<form action="index.html">
<input type="submit" value="Zur�ck zur Suche" />
</form>
</div>
</body>
</html>