package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.shop.Warenkorb;
import model.shop.command.Command;
import model.shop.command.CommandBroker;
import model.shop.command.CommandContext;

@WebServlet("/shopcontrol")
public class ShopControl extends HttpServlet {
	private static final long serialVersionUID = 1200L;
	private CommandBroker broker = null;

	public ShopControl() {
		super();
	}
	
	@Override
	public void init() throws ServletException {
		super.init();
		this.broker = CommandBroker.getInstance();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		Warenkorb wk = (Warenkorb) session.getAttribute("warenkorb");
		if (wk == null) {
			wk = new Warenkorb();
			session.setAttribute("warenkorb", wk);
		}
		
		request.setAttribute("warenkorb", wk);
		
		String commandName = request.getParameter("command");
		if (commandName == null) {
			Command command = this.broker.lookupCommand("start");
			session.setAttribute("__command", command);
			getServletContext().getRequestDispatcher(command.getView()).forward(request, response);
		}
		
		Command previous = (Command) session.getAttribute("__command");
		Command command = broker.lookupCommand(commandName);
		
		CommandContext context = this.createContext(request);
		context.setWarenkorb(wk);
		
		if (previous !=null && previous.isAllowedSuccessor(command.getName()) == false) {
			this.copyContextIntoRequest(context, request);
			getServletContext().getRequestDispatcher(previous.getView()).forward(request, response);
			return;
		} else {
			command = command.executeCommand(context);
			this.copyContextIntoRequest(context, request);
			
			if (context.hasError()) {
			getServletContext().getRequestDispatcher(previous.getView()).forward(request, response);
			} else {
				session.setAttribute("__command", command);
				getServletContext().getRequestDispatcher(command.getView()).forward(request, response);
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}
	
	private CommandContext createContext(HttpServletRequest request) {
		CommandContext context = new CommandContext();
		
		Map<String, String[]> paramMap = request.getParameterMap();
		for (String key : paramMap.keySet() ) {
			String[] values = paramMap.get(key);
//			if (values.length == 1) {
//				context.setAttribute(key, values[0]);
//			}
			// Unterstützung für mehrwertige Attribute wie Zutaten
			//context.setAttribute(key, values);
			List<String> vals = new ArrayList<>();
			for (String s : values) {
				vals.add(s);
			}
			context.setAttribute(key, vals);
		}
		
		Object user = request.getSession().getAttribute("user");
		if (user != null) {
			context.setAttribute("user", user);
		}
		return context;
	}
	
	private void copyContextIntoRequest(CommandContext context, HttpServletRequest request) {
		for (String key : context.getAttributeNames()) {
			Object value = context.getAttribute(key);
			request.setAttribute(key, value);
		}
		
		request.setAttribute("warenkorb", context.getWarenkorb());
		
		if (context.hasError()) {
			request.setAttribute("error", context.getError());
		}
		
		if (context.getAttribute("user") != null) {
			request.getSession().setAttribute("user", context.getAttribute("user"));
		}
		
		return;
	}
	
}
