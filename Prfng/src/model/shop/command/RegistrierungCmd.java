package model.shop.command;

import model.shop.Warenkorb;

public class RegistrierungCmd extends Command {
	
	public RegistrierungCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
	}

	@Override
	public void execute(CommandContext context) {
		Warenkorb wk = (Warenkorb) context.getAttribute("warenkorb");
		context.setAttribute("warenkorb", wk);

	}

}
