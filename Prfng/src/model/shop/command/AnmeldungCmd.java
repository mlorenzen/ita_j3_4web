package model.shop.command;

public class AnmeldungCmd extends Command {

	public AnmeldungCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
	}

	@Override
	public void execute(CommandContext context) {
		if (context.getAttribute("user") != null) {
			context.setForwardCommand("kaufen");
		}
		
		
	}

}
