package model.shop;

import java.util.ArrayList;
import java.util.List;

public class ShopFacade {
	
	private static class Instance {
		private static ShopFacade instance = new ShopFacade();
	}
	
	public static ShopFacade getInstance() {
		return Instance.instance;
	}
	
	private List<Pizza> pizzaList = new ArrayList<Pizza>();
	
	private ShopFacade() {
		super();
	}

	public PizzaOrder getNewOrder(String size, List<String> zutaten, int lastID) {
		Pizza pizza = new Pizza();
		pizza.setSize(PizzaSize.valueOf(size));
		if (zutaten != null) {
			for (String z : zutaten) {
				pizza.addIngredient(Ingredients.valueOf(z));
			}
		}
		
		PizzaOrder order = new PizzaOrder(lastID + 1, pizza);
		return order;
	}

}
