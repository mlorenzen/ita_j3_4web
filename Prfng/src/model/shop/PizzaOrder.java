package model.shop;

import java.io.Serializable;

public class PizzaOrder implements Serializable {
	private static final long serialVersionUID = 32345009L;
	
	private int id; // muss eindeutig sein
	private Pizza pizza;
	private int count;
	
	public PizzaOrder() {
		super();
	}
	
	public PizzaOrder(int id, Pizza pizza) {
		super();
		this.id = id;
		this.pizza = pizza;
		this.count = 1;
	}
	
	public void addPizza(int id, Pizza pizza) {
		if (this.pizza == null) {
			this.id = id;
			this.pizza = pizza;
			this.count = 1;
		}
	}
	
	public void increaseCount() {
		count += 1;
	}

	public void decreaseCount() {
		count = (count >= 1) ? count-= 1 : 0;
	}
	
	public void setCount(int c) {
		count = (c >= 0) ? c : count;
	}
	
	public double getPrice() {
		return this.pizza.getPrice() * count;
	}
	
	public int getCount() {
		return this.count;
	}
		
	public Pizza getPizza() {
		return this.pizza;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getPizzaDescription() {
		return this.pizza.toString();
	}
}
