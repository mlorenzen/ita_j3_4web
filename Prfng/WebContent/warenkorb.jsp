<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<title>Michas Pizzaladen - Warenkorb</title>
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Ihr Warenkorb</h2>
<table >
<tr>
  <th>Nr.</th>
  <th>Anzahl</th>
  <th>Pizza</th>
  <th>Preis</th>
  <th>�ndern</th>
</tr>
<c:forEach items="${warenkorb.content}" var="b">
  <tr>
    <td>${b.id }</td>
    <td>${b.count } </td>
    <td>${b.pizzaDescription }</td>
    <td>${b.price }</td>
    <td>
      <form action="shopcontrol" method="get">
      <input type="hidden" name="command" value="changeOrder">
      <input type="hidden" name="orderNr" value="${b.id }">
         <button name="changeCmd" value="inc" class="buttnWk" title="Anzahl erh�hen"><img src="img/plus.png" alt="+"></button> 
         <button name="changeCmd" value="dec" class="buttnWk" title="Anzahl verringern"><img src="img/minus.png" alt="-"></button>
         <button name="changeCmd" value="del" class="buttnWk" title="Pizza l�schen"><img src="img/trash.png" alt="+" ></button>
     </form>
    </td>
  </tr>
</c:forEach>
</table>
<p>Gesamtpreis: ${warenkorb.price } EUR</p>
<hr>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="auswahl">
  <input class="buttn" type="submit" value="Mehr Pizzen ausw�hlen">
</form>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="anmeldung">
  <input class="buttn" type="submit" value="Zur Kasse">
</form>
<hr>
<p>Ihr Benutzername: ${user} </p> 
</div>
</body>
</html>