window.onload = function() {
	document.getElementById("input_mail").onblur = validateEmail;
	document.getElementById("input_password").onkeyup = evaluatePassword;
	document.getElementById("input_password").onfocus = clearPasswordMsg;
	document.getElementById("input_plz").onblur = getPlace;
	document.getElementById("input_plz").onkeydown = removePlace;
}

function getPlace(){
	var currentPlz = document.getElementById("input_plz").value;
	var plzRegEx = /[0-9]{5}/;
	var inputOrt = document.getElementById("input_ort");
	var httpRequest = new XMLHttpRequest();
	
	if (plzRegEx.test(currentPlz) == true){
		httpRequest.onreadystatechange = function(e) {
			if (e.target.readyState === XMLHttpRequest.DONE){
				var places = e.target.responseText.split(";");
				var firstPlace = places[0].substring(5, places[0].length).trim();
				inputOrt.value = firstPlace;
			}
		};
		
		var url = "./ort?plz=" + currentPlz;
		httpRequest.open("GET", url);
		httpRequest.send();
		
	} else {
		inputOrt.value = "";
	}
}

function removePlace() {
	document.getElementById("input_ort").value = "";
}

function validateEmail() {
	var mailAddress = document.getElementById("input_mail").value; // Eingabe ermitteln
	var regEx = /^[a-z]{4}[0-9]{4}@stud\.(hs|fh)-kl\.de$/; // Prüfausdruck
	var mailInputPos = document.getElementById("input_mail")
			.getBoundingClientRect(); // Positon des Eingabefelds

	// div für Fehlerausgabe erzeugen
	var mailErrorDiv = document.getElementById("mailErrorDiv");
	if (mailErrorDiv === null) {
		mailErrorDiv = document.createElement("div");
		mailErrorDiv.id = "mailErrorDiv";
		mailErrorDiv.style.position = 'absolute';
		mailErrorDiv.style.top = (mailInputPos.top + 2) + "px";
		mailErrorDiv.style.left = (mailInputPos.right + 5) + "px";
		mailErrorDiv.innerHTML = "E-Mail-Adresse ist nicht zulässig";
		mailErrorDiv.style.color = "red";
		mailErrorDiv.style.visibility = "hidden";
		document.body.appendChild(mailErrorDiv);
	}

	if (regEx.test(mailAddress) == false) {
		// Fehler-Div anzeigen
		mailErrorDiv.style.visibility = "visible";
	} else {
		// Fehler-Div löschen
		mailErrorDiv.style.visibility = "hidden";
	}
}

function evaluatePassword() {
	var currentPassword = document.getElementById("input_password").value;
	var passwdInputPos = document.getElementById("input_password").getBoundingClientRect();
	
	function passwdLength(password) {
		if (password.length > 5) {
			return 4;
		} else {
			return 0; 
		}
	}

	function passwdSpecialChar(password) {
		var specialCharRegEx = /(!|§|\$|&|\?)/;
		if (specialCharRegEx.test(password) == true) {
			return 2;
		} else {
			return 0;
		}
	}
	
	function passwdDigits(password) {
		var digitsRegEx = /[0-9][^0-9]+[0-9]/;
		if (digitsRegEx.test(password) == true) {
			return 1;
		} else {
			return 0;
		}
		
	}

	
	var passwdStrengthDiv = document.getElementById("passwdStrengthDiv");
	if (passwdStrengthDiv === null){
		passwdStrengthDiv = document.createElement("div");
        passwdStrengthDiv.id = "passwdStrengthDiv";
		passwdStrengthDiv.style.position = 'absolute';
		passwdStrengthDiv.style.top = (passwdInputPos.top + 2) + "px";
		passwdStrengthDiv.style.left = (passwdInputPos.right + 5) + "px";
		passwdStrengthDiv.innerHTML = "";
		passwdStrengthDiv.style.visibility = "hidden";
		document.body.appendChild(passwdStrengthDiv);
	}
	
	var passwdStrength = passwdLength(currentPassword) + passwdSpecialChar(currentPassword) + passwdDigits(currentPassword);
	
	if (passwdStrength > 6) {
		passwdStrengthDiv.innerHTML = "Passwort ist sehr sicher";
		passwdStrengthDiv.style.color = "green";
	} else if (passwdStrength > 4) {
		passwdStrengthDiv.innerHTML = "Passwort ist sicher";
		passwdStrengthDiv.style.color = "lightgreen";
	} else if (passwdStrength > 3) {
		passwdStrengthDiv.innerHTML = "Passwort ist akzeptabel";
		passwdStrengthDiv.style.color = "orange";
	} else {
		passwdStrengthDiv.innerHTML = "Passwort ist unsicher";
		passwdStrengthDiv.style.color = "red";
	} 
	passwdStrengthDiv.style.visibility = "visible";
}	


function clearPasswordMsg() {
	if (!document.getElementById("passwdStrengthDiv")) {
		document.getElementById.visibility = "hidden";
	}
}