window.onload = function() {

	var bookLinks = document.getElementsByTagName('a');
	for (var i = 0, len = bookLinks.length; i < len; i++){
		bookLinks[i].onmouseover = showImage;
		bookLinks[i].onmouseout = removeImage;
	}
	
}

	function showImage(event){
		var tableHeadRect = document.getElementById("tableHeader").getBoundingClientRect();

		var imageNr = event.target.id;
		
		var coverDiv = document.getElementById("coverDiv");
		if (coverDiv === null){
			coverDiv = document.createElement("div");
	        coverDiv.id = "coverDiv";
			coverDiv.style.position = 'absolute';
			coverDiv.style.top = (tableHeadRect.bottom + 2) + "px";
			//coverDiv.style.left = (tableHeadRect.right + 15) + "px";
			coverDiv.style.right = "15px";
			coverDiv.innerHTML = "";
			coverDiv.style.visibility = "hidden";
			document.body.appendChild(coverDiv);
		}
		coverDiv.innerHTML = "";
		coverDiv.innerHTML = "<img alt='cover' src='pic/" + imageNr + ".jpg' />";
		coverDiv.style.visibility = "visible";
		
	}
	
	function removeImage() {
		var coverDiv = document.getElementById("coverDiv");
		coverDiv.style.visibility = "hidden";
	
}