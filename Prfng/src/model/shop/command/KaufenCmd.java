package model.shop.command;

import java.util.ArrayList;

import model.shop.Warenkorb;
import model.user.AccountFacade;

public class KaufenCmd extends Command {

	public KaufenCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CommandContext context) {

		ArrayList<String> userL = (ArrayList<String>) context.getAttribute("username");

		if (userL != null) {
			String user = userL.get(0);
			String password = ((ArrayList<String>) context.getAttribute("passwd")).get(0);

			AccountFacade facade = AccountFacade.getInstance();
			boolean validLogin = facade.validatePassword(user, password);

			if (validLogin) {

				context.setAttribute("user", user);
			} else {
				context.setError(new Exception("Keine gültigen Logindaten"));
				context.setAttribute("error", "Keine gültigen Logindaten");
			}
		}

	}

}
