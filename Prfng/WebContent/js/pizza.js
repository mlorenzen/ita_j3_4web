window.onload = function() {
	var zutaten = document.querySelectorAll('input[name="zutat"]');
	for (var i = 0, len = zutaten.length; i < len; i++) {
		zutaten[i].onchange = updatePrice;
	}
	
	document.getElementById("sizeSelect").onchange = updatePrice;
	updatePrice();
		
}

function updatePrice(){

	var price = 0.0;
	var ausgabe = document.getElementById("preisanzeige");

	var sizeValue = document.getElementById("sizeSelect").value;
	switch (sizeValue) {
	case "S":
		price = 4.0;
		break;
	case "M":
		price = 5.0;
		break;
	case "L":
		price = 6.0;
		break;
	}
	
	var anzahl = 0;
	var zutaten = document.querySelectorAll('input[name="zutat"]');
	for (var i = 0, len = zutaten.length; i < len; i++) {
		if (zutaten[i].checked) {
			anzahl++;
		}
	}
	
	price += (0.5 * anzahl);
	
	
	ausgabe.innerHTML = price + " EUR";
	
}