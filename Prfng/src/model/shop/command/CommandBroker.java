package model.shop.command;

import java.util.HashMap;
import java.util.Map;

public class CommandBroker {
	
	private static class CommandBrokerHolder {
		private static CommandBroker instance = new CommandBroker();
	}
	
	private Map<String, Command> commands = new HashMap<String, Command>();
	
	private CommandBroker() {
		super();
		this.buildGraph();
	}
	
	public static CommandBroker getInstance() {
		return CommandBrokerHolder.instance;
	}
	
	public Command lookupCommand(String s) {
		return this.commands.get(s);
	}
	
	private void addCommand(Command c) {
		this.commands.put(c.getName(), c);
	}
	
	private void buildGraph() {
		this.addCommand(new NeustartCmd("neustart", "/index.jsp", "auswahl"));
		this.addCommand(new AuswahlCmd("auswahl", "/pizza.jsp", "warenkorb"));
		this.addCommand(new WarenkorbCmd("warenkorb", "/warenkorb.jsp", "auswahl", "anmeldung",
				"changeOrder"));
		this.addCommand(new ChangeWkCmd("changeOrder", "/warenkorb.jsp", "changeOrder", "anmeldung", "auswahl"));
		this.addCommand(new EditWkCmd("editWarenkorb", "/warenkorb.jsp", "changeOrder", "anmeldung", "auswahl"));
		this.addCommand(new AnmeldungCmd("anmeldung", "/anmeldung.jsp", "registrierung", "kaufen"));
		this.addCommand(new RegistrierungCmd("registrierung", "/registrierung.jsp", "register", "editWarenkorb"));
		this.addCommand(new RegisterCmd("register", "/kaufen.jsp", "auswahl", "danke", "editWarenkorb"));
		this.addCommand(new KaufenCmd("kaufen", "/kaufen.jsp", "auswahl", "danke", "editWarenkorb"));
		this.addCommand(new DankeCmd("danke", "/danke.jsp", "neustart"));
	}

}
