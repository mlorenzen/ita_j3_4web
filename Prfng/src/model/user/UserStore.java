package model.user;

import java.util.HashMap;
import java.util.Map;

public class UserStore {
	
	private static class Instance {
		private static UserStore instance = new UserStore();
	}
	
	public static UserStore getInstance() {
		return Instance.instance;
	}
	
	private Map<String, User> userMap = new HashMap<>();
	
	private UserStore() {
		super();
		this.initDefaultUser();
	}
	
	public void addUser(String username, String firstname, String name, String address,
			int plz, String city, String telephone, String password) {
		if (!userMap.containsKey(username)) {
			userMap.put(username, new User(firstname, name, address, plz, city, telephone, password));
		} //TODO: Fehler melden falls username schon vergeben
	}
	
	public boolean testPassword(String username, String password) {
		if (userMap.get(username) == null) return false;
		if (userMap.get(username).getPassword().equals(password)) {
			return true;
		}
		return false;
	}
	
	public User getUser(String username) {
		return userMap.get(username); //null falls nicht vorhanden
	}
	
	private void initDefaultUser() {
		this.addUser("benutzer1", "Vorname1", "Name1", "Straße1", 11111, "Stadt1",
				"11 1111 1111", "1111");
		this.addUser("benutzer2", "Vorname2", "Name2", "Straße2", 22222, "Stadt2",
				"22 2222 2222", "2222");
	}

}
