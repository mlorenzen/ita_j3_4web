package model.shop;

public enum PizzaSize {
	
	S, M, L;
	
	@Override
	public String toString() {
		
		int o = ordinal();
		switch ( o ) {
		case 0:
			return "klein";
		case 1:
			return "mittel";
		case 2:
			return "groß";
		}
		return null;
	}

}
