package model.shop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Warenkorb implements Serializable {
	
	private static final long serialVersionUID = 39932488L;

	// TODO:
	// -> Liste der Zutaten ausgeben für Werbung
	// -> Liste der Pizzen verwalten
	// -> Änderungen an Anzahl erlauben / Löschen erlauben
	
	private List<PizzaOrder> orderList = new ArrayList<>();
	
	public Warenkorb() {
		super();
	}

	 public void addOrder(PizzaOrder order) {
		 this.orderList.add(order);
	 }
	 
	 public void removeAll() {
		 this.orderList = new ArrayList<PizzaOrder>();
	 }
	 
	 public Collection<PizzaOrder> getContent() {
		 return new ArrayList<PizzaOrder>(this.orderList);
	 }
	 
	 public double getPrice() {
		 double price = 0.0;
		 
		 for (PizzaOrder order : orderList) {
			 price += order.getPrice();
		 }
		 return price;
	 }
	 
	 public int getOrderCount() {
		 return orderList.size();
	 }
	 
	 public void increaseOrder(String orderIdAsString) {
		 int orderID = Integer.parseInt(orderIdAsString);
		 for (PizzaOrder order: orderList) {
			 if (order.getId() == orderID) {
				 order.increaseCount();
			 }
		 }
	 }
	 
	 public void decreaseOrder(String orderIdAsString) {
		 int orderID = Integer.parseInt(orderIdAsString);
		 for (PizzaOrder order: orderList) {
			 if (order.getId() == orderID) {
				 if (order.getCount() > 1) {
					 order.decreaseCount();
					 return;
				 }
			 }
		 }
		 // count == 1: also können wir löschen
		 deleteOrder(orderIdAsString);
	 }
	 
	 public void deleteOrder(String orderIdAsString) {
		 int orderID = Integer.parseInt(orderIdAsString);
		 PizzaOrder orderToDelete = null;
		 for (PizzaOrder order : orderList) {
			 if (order.getId() == orderID) {
				 orderToDelete = order;
			 }
		 }
		 orderList.remove(orderToDelete);
	 }
	 
}
