package model.shop.command;

import java.util.ArrayList;
import java.util.List;

import model.shop.PizzaOrder;
import model.shop.ShopFacade;

public class WarenkorbCmd extends Command {

	private ShopFacade facade = null;

	public WarenkorbCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
		this.facade = ShopFacade.getInstance();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CommandContext context) {

		String size = ((ArrayList<String>) context.getAttribute("pizzaSize")).get(0);
		List<String> zutaten = new ArrayList<String>();
		zutaten = (ArrayList<String>) context.getAttribute("zutat");
		
		//ID der letzten Order ermitteln
		int nrOrders = context.getWarenkorb().getContent().size();
		int lastID = 0;
		if (nrOrders > 0) {
			lastID = ((PizzaOrder) context.getWarenkorb().getContent().toArray()[nrOrders - 1]).getId();
		}

		PizzaOrder order = this.facade.getNewOrder(size, zutaten, lastID);

		context.getWarenkorb().addOrder(order);
		
		

	}

}
