package michaelLorenzen;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.shop.Buch;
import model.shop.ModelFacade;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet(description = "Servlet für die Büchersuche", urlPatterns = { "/suche" })
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1000L;
	
	private ModelFacade facade = null;
       
    public SearchServlet() {
        super();
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	this.facade = ModelFacade.getInstance();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String autor = request.getParameter("autorname");
		Collection<Buch> books = this.facade.findBuecherFromAutor(autor);
		
		request.setAttribute("buecher", books);
		
		getServletContext().getRequestDispatcher("/suchergebnis.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
