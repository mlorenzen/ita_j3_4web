package model.shop.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.user.AccountFacade;

public class RegisterCmd extends Command {

	public RegisterCmd(String name, String view, String... cmds) {
		super(name, view, cmds);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CommandContext context) {

		Map<String, String> newUser = new HashMap<String, String>();
		AccountFacade facade = AccountFacade.getInstance();

		List<String> userAttributes = Arrays.asList("vorname", "name", "address", "plz", "city", "telephone",
				"password");

		String username = ((ArrayList<String>) context.getAttribute("username")).get(0);

		if (facade.userExists(username)) {
			context.setError(new Exception("Benutzername ist schon vorhanden"));
			context.setAttribute("error", "Benutzername ist schon vorhanden");
		} else if (!((ArrayList<String>) context.getAttribute("password")).get(0)
				.equals(((ArrayList<String>) context.getAttribute("pw2")).get(0))) {
			context.setError(new Exception("Passwörter sind nicht gleich"));
			context.setAttribute("error", "Passwörter sind nicht gleich");
		} else {
			for (String attribut : userAttributes) {
				String value = ((ArrayList<String>) context.getAttribute(attribut)).get(0);
				if (value.equals("")) {
					context.setError(new Exception("Alle Felder müssen gefüllt sein"));
					context.setAttribute("error", "Alle Felder müssen gefüllt sein");
					return;
				} else {
					newUser.put(attribut, ((ArrayList<String>) context.getAttribute(attribut)).get(0));
					newUser.put("username", username);
				}
			}
			facade.saveUser(newUser);
			context.setAttribute("user", username);
		}

	}
}