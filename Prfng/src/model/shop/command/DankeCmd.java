package model.shop.command;

import model.user.AccountFacade;

public class DankeCmd extends Command {

	public DankeCmd(String name, String view, String... cmds){
		super(name, view, cmds);
	}
	
	@Override
	public void execute(CommandContext context) {
		
		String username = (String) context.getAttribute("user");
		
		AccountFacade facade = AccountFacade.getInstance();
		
		String deliveryAddress = facade.getDeliveryAddress(username);
		
		context.setAttribute("deliveryAddress", deliveryAddress);

	}

}
