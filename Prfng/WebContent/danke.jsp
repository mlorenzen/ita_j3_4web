<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache, must-revalidate, no-store">
<meta http-equiv="pragma" content="no-cache">
<title>Michas Pizzaladen - Danke</title>
<link rel="stylesheet" type="text/css" href="css/pizzaML.css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<div id="content">
<h1>Michas Pizzaladen</h1>
<h2>Vielen Dank f�r Ihre Bestellung</h2>
<p>Wir werden Ihre Pizza sorgf�ltig zubereiten und sie Ihnen so schnell wie m�glich zustellen.<br>
  Ihre gespeicherte Adresse lautet: ${deliveryAddress }</p>
<form action="shopcontrol" method="get">
  <input type="hidden" name="command" value="neustart">
  <input class="buttn" type="submit" value="Zur�ck zur Startseite">
</form>
</div>
<jsp:scriptlet> session.invalidate(); </jsp:scriptlet>
</body>
</html>