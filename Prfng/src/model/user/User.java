package model.user;

public class User {
	
	private String firstName;
	private String name;
	private String address;
	private Integer plz;
	private String city;
	private String telephone;
	private String password;
	
	public User() {
		super();
	}

	public User(String firstName, String name, String address, int plz, String city, String telephone, String password) {
		super();
		this.firstName = firstName;
		this.name = name;
		this.address = address;
		this.plz = plz;
		this.city = city;
		this.telephone = telephone;
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getName() {
		return name;
	}

	public String getFullAddress() {
		return address + ", " + plz.toString() + " " + city;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getPassword() {
		return password;
	}
	
	

}
