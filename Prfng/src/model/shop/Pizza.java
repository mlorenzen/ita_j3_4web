package model.shop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pizza implements Serializable {
	
	private static final long serialVersionUID = 423546L;
	private PizzaSize size;
	private List<Ingredients> ingredients = new ArrayList<Ingredients>();
	
	public Pizza() {
		super();
	}
	
	public Pizza(int id, PizzaSize size, List<Ingredients> ingredients) {
		super();
		this.size = size;
		this.ingredients = ingredients;
	}
	
	public void setSize(PizzaSize s) {
		this.size = s;
	}
	
	public PizzaSize getSize() {
		return this.size;
	}
	
	public void addIngredient(Ingredients i) {
		this.ingredients.add(i);
	}
	
	public List<Ingredients> getIngredients(){
		return this.ingredients;
	}
	
	public double getPrice() {
		double price = 0.0;
		
		// Preis der Grundpizza
		PizzaSize s = this.size;
		switch (s) {
		case S:
			price += 4;
			break;
		case M:
			price += 5;
			break;
		case L:
			price += 6;
			break;
		}
		
		// Jede Extra Zutat kostet 0,50
		price += (this.ingredients.size() * 0.5);
		
		return price;
		
	}
	
	@Override
	public String toString() {
		StringBuilder text = new StringBuilder();
		text.append("Größe ");
		text.append(size.name());
		text.append(", Zutaten:");
		for (Ingredients i: ingredients) {
			text.append(" ");
			text.append(i.toString());
		}
		
		return text.toString();
	}

}
